class CreateFunctions:
	def __init__(self, f, usb_id, functions_ci, functions_co, functions_bi, functions_bo, trace):
		self._f = open(f, 'r')
		self._usb_id = usb_id
		self._functions_ci = open(functions_ci+".c", 'w')
		self._functions_co = open(functions_co+".c", 'w')
		self._functions_ci_h = open(functions_ci+".h", 'w')
		self._functions_co_h = open(functions_co+".h", 'w')
		self._functions_bi = open(functions_bi+".c", 'w')
		self._functions_bi_h = open(functions_bi+".h", 'w')
		self._functions_bo = open(functions_bo+".c", 'w')
		self._functions_bo_h = open(functions_bo+".h", 'w')
		self._trace_c = open(trace+".c", 'w')
		self._trace_h = open(trace+".h", 'w')
		self._functions=[""]
		self._trace=[""]

	def _create_ci_function(self,pattern):
		print "ci create"
		requesttype=pattern[5]
		request=pattern[6]
		value=pattern[7]
		index=pattern[8]
		bytes=pattern[10]
		timeout=5000

		function_name="f_ci_"+"request_"+request+"_value_"+value+"_index_"+index+"_bytes_"+bytes

		if not function_name in self._functions:
			self._functions.append(function_name)
			#c source
			self._functions_ci.write("\nvoid "+function_name+"(usb_dev_handle *dev) {\n")
			self._functions_ci.write("\tint ret;\n")
			self._functions_ci.write("\tchar buffer[512];\n")
			self._functions_ci.write("\tret = usb_control_msg(dev, 0x"+requesttype+", 0x"+request+", 0x"+value+", 0x"+index+", buffer, 0x"+bytes+", "+str(timeout)+");"+"\n")
			self._functions_ci.write("\tif (ret >=0)\n")
			self._functions_ci.write("\t\tprintf(\"read buffer %s ret: %i\\n\",buffer,ret);\n")
			self._functions_ci.write("\telse\n")
			self._functions_ci.write("\t\tprintf(\"error code no %i ret\\n\",ret);\n")
			self._functions_ci.write("}\n")
			#c header
			self._functions_ci_h.write("\nvoid "+function_name+"(usb_dev_handle *dev);\n")

		self._trace_c.write("\n\t"+function_name+"(dev);\n")

	def _create_co_function(self,pattern):
		print "co create"
		requesttype=pattern[5]
		request=pattern[6]
		value=pattern[7]
		index=pattern[8]
		bytes=pattern[10]
		timeout=5000
		i=(int(bytes)-1)/4
		buf=""
		while i >= 0: 
			buf=pattern[12+i] + buf
			i=i-1
		print buf

		function_name="f_co_"+"request_"+request+"_value_"+value+"_index_"+index+"_bytes_"+bytes
		if not function_name in self._functions:
			self._functions.append(function_name)
			#c source
			self._functions_co.write("\nvoid "+function_name+"(usb_dev_handle *dev,char * buffer) {\n")
			self._functions_co.write("\tint ret;\n")
			self._functions_co.write("\tret = usb_control_msg(dev, 0x"+requesttype+", 0x"+request+", 0x"+value+", 0x"+index+", buffer, 0x"+bytes+", "+str(timeout)+");"+"\n")
			self._functions_co.write("\tif (ret >=0)\n")
			self._functions_co.write("\t\tprintf(\"write buffer %s ret: %i\\n\",buffer,ret);\n")
			self._functions_co.write("\telse\n")
			self._functions_co.write("\t\tprintf(\"error code no %i ret\\n\",ret);\n")
			self._functions_co.write("}\n")
			#c header
			self._functions_co_h.write("\nvoid "+function_name+"(usb_dev_handle *dev,char * buffer);\n")

		self._trace_c.write("\n\tmemset(buffer,0x0,512);\n")
		i=0
		while not i >= int(bytes)*2:
			self._trace_c.write("\tbuffer["+str(i/2)+"]=0x"+buf[i:i+2]+";\n")
			i=i+2
		self._trace_c.write("\t"+function_name+"(dev,buffer);\n")

	def _begin_trace_skeleton(self):
		#c source
		self._trace_c.write("#include \"usbtransfer.h\"\n")
		self._trace_c.write("#include \"functions_ci.h\"\n")
		self._trace_c.write("#include \"functions_co.h\"\n")
		self._trace_c.write("\n")
		self._trace_c.write("void play_trace(usb_dev_handle *dev) {\n")
		self._trace_c.write("\tchar buffer[512];\n")
		#c header
		self._trace_h.write("#ifndef TRACE_H\n")
		self._trace_h.write("#define TRACE_H\n")
		self._trace_h.write("\n")
		self._trace_h.write("void play_trace(usb_dev_handle *dev);\n")

	def _end_trace_skeleton(self):
		self._trace_c.write("}")
		self._trace_h.write("#endif")

	def _begin_functions_ci_skeleton(self):
		#c source
		self._functions_ci.write("#include \"usbtransfer.h\"\n")
		self._functions_ci.write("\n")
		#c header
		self._functions_ci_h.write("#ifndef FUNCTIONS_CI_H\n")
		self._functions_ci_h.write("#define FUNCTIONS_CI_H\n")

	def _end_functions_ci_skeleton(self):
		self._functions_ci_h.write("#endif")

	def _begin_functions_co_skeleton(self):
		#c source
		self._functions_co.write("#include \"usbtransfer.h\"\n")
		self._functions_co.write("\n")
		#c header
		self._functions_co_h.write("#ifndef FUNCTIONS_CO_H\n")
		self._functions_co_h.write("#define FUNCTIONS_CO_H\n")

	def _end_functions_co_skeleton(self):
		self._functions_co_h.write("#endif")

	def _begin_functions_bi_skeleton(self):
		#c source
		self._functions_bi.write("#include \"usbtransfer.h\"\n")
		self._functions_bi.write("\n")
		#c header
		self._functions_bi_h.write("#ifndef FUNCTIONS_BI_H\n")
		self._functions_bi_h.write("#define FUNCTIONS_BI_H\n")

	def _end_functions_bi_skeleton(self):
		self._functions_bi_h.write("#endif")

	def _begin_functions_bo_skeleton(self):
		#c source
		self._functions_bo.write("#include \"usbtransfer.h\"\n")
		self._functions_bo.write("\n")
		#c header
		self._functions_bo_h.write("#ifndef FUNCTIONS_BO_H\n")
		self._functions_bo_h.write("#define FUNCTIONS_BO_H\n")

	def _end_functions_bo_skeleton(self):
		self._functions_bo_h.write("#endif")

	def begin_skeleton(self):
		#FIXME: generalize the code ;)
		self._begin_trace_skeleton()
		self._begin_functions_ci_skeleton()
		self._begin_functions_co_skeleton()
		self._begin_functions_bi_skeleton()
		self._begin_functions_bo_skeleton()

	def end_skeleton(self):
		#FIXME: generalize the code ;)
		self._end_trace_skeleton()
		self._end_functions_ci_skeleton()
		self._end_functions_co_skeleton()
		self._end_functions_bi_skeleton()
		self._end_functions_bo_skeleton()

	def read_file(self):
		for line in self._f:
			s = line.split()
			if s[2]=="S":
				if s[3]=="Ci:"+self._usb_id: #if s[3].split(':')[0]=="Ci":
					#print line
					self._create_ci_function(s)
				if s[3]=="Co:"+self._usb_id: #if s[3].split(':')[0]=="Co":
					self._create_co_function(s)

create_functions=CreateFunctions('../sniffed/sinit_fixed','3:003:0','functions_ci','functions_co', 'functions_bi' , 'functions_bo' , 'trace')
create_functions.begin_skeleton()
create_functions.read_file()
create_functions.end_skeleton()
