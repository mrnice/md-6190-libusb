#include "usbtransfer.h"
#include "trace.h"
#include "functions_ci.h"
#include "functions_co.h"

int main(void) {
	usb_dev_handle *dev = NULL; /* the device handle */
	uint8_t read_buffer[512];
	int ret =0;

	/*Set up USB */
	usb_init();
	usb_find_busses();
	usb_find_devices();

	if (!(dev = open_dev())) {
	perror("device not found!\n");
		exit(EXIT_FAILURE);
	}

	if (usb_claim_interface(dev, 0) < 0) {
		perror("claiming interface failed\n");
		usb_close(dev);
		exit(EXIT_FAILURE);
	}

	/* start the playback */
	play_trace(dev);

	/* Clean up */
	usb_release_interface(dev, 0);
	usb_close(dev);
	exit(0);
}
