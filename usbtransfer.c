#include "usbtransfer.h"

usb_dev_handle *open_dev(void) {
	struct usb_bus *bus;
	struct usb_device *dev; 
	for (bus = usb_get_busses(); bus; bus = bus->next){
		for (dev = bus->devices; dev; dev = dev->next){
			if (dev->descriptor.idVendor == MD_6190_VID && dev->descriptor.idProduct == MD_6190_PID)
				return usb_open(dev);
		}
	}
	return NULL;
}
