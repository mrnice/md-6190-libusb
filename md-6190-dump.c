#include "usbtransfer.h"

#define C_OUT 0x40
#define C_IN 0xc0

void print_buffer(uint8_t*  buffer, int len) {
	int i;
	printf("buffer 0x :");
	for(i=0;i<len;i++) {
		printf(" %X ", buffer[i]);
	}
	printf("\n");
}

void print_supposed_matrix(uint8_t*  buffer, int len) {
	/* buffer length ist 5 for one point and 10 for more points
	 * a typical press and release cycle looks like this
	 * buffer 0x : 81  6c  6  18  7 
	 * buffer 0x : 80  6c  6  18  7 
	 * the first byte could indecates touch press or release
	 * so what are the input vectors for x y?
	 * I assume that col: 2 is x and col: 4 is y
	 * The range is between 0 and F upper left is 0,0
	 * and lower right is F,F
	 * A second Pointer is tracked within another block of 5 byte
	 * starting with B1 or B0 (press/release)
	 */

	printf("Pointer ONE:  %s ",buffer[0]==0x81 ? "PRESS":"RELEASE");
	printf("x: %i y: %i\n",buffer[2],buffer[4]);

	if(len>5) {
		printf("Pointer TWO:  %s ",buffer[5]==0xB1 ? "PRESS":"RELEASE");
		printf("x: %i y: %i\n",buffer[7],buffer[9]);
	}
}


/*
eb65b600 2957619653 S Ci:3:002:0 s c0 04 0040 0000 0004 4 <
eb65b600 2957621301 C Ci:3:002:0 0 4 = 00000000
eb65b600 2957622373 S Co:3:002:0 s 40 0c 0040 0000 0001 1 = 00
eb65b600 2957624311 C Co:3:002:0 0 1 >
eb65b600 2957625024 S Ci:3:002:0 s c0 0c 00e1 ffe2 0001 1 <
eb65b600 2957626293 C Ci:3:002:0 0 1 = bf
eb65b600 2957627078 S Ci:3:002:0 s c0 0c 00e1 fdf5 0001 1 <
eb65b600 2957628300 C Ci:3:002:0 0 1 = 00
eb65b600 2957647272 S Ci:3:002:0 s c0 04 0040 0000 0004 4 <
eb65b600 2957649303 C Ci:3:002:0 0 4 = 00000000
*/
/*
eb64b000 2861903934 S Co:1:004:0 s 40 0c 0040 0000 0001 1 = 00
eb64b000 2861904520 C Co:1:004:0 0 1 >
*/

void print_CiF1(usb_dev_handle *dev) {
	/*
	 * #CiF1
	 * S Ci:3:002:0 s c0 04 0040 0000 0004 4 <
	 * C Ci:3:002:0 0 4 = 00000000
	 */
	int requesttype = C_IN;
	int request =  0x04;
	int value = 0x0040;
	int index = 0x0;
	char buffer[512];
	int bytes = 4;
	int timeout = 5000;
	int ret;

	ret = usb_control_msg(dev, requesttype, request, value, index, buffer, bytes, timeout);
	if (ret >=0)
		printf("read buffer %s ret: %i\n",buffer,ret);
	else
		printf("error code no %i ret\n",ret);
}

/**********************Co functions********************************************/
void print_COF1(usb_dev_handle *dev) {
	/*
	 * 40 0c 0040 0000 0001 1
	 */
	int requesttype = C_OUT;
	int request =  0x0c;
	int value = 0x0040;
	int index = 0x0;
	char buffer[512];
	int bytes = 1;
	int timeout = 5000;
	int ret;

	ret = usb_control_msg(dev, requesttype, request, value, index, buffer, bytes, timeout);
	if (ret >=0)
		printf("read buffer %s ret: %i\n",buffer,ret);
	else
		printf("error code no %i ret\n",ret);
}

void print_COF2(usb_dev_handle *dev) {
	/*
	 * #CoF2
	* S Co:3:002:0 s 40 04 00d0 0000 0004 4 = 20008000
	* C Co:3:002:0 0 4 >
	 */
	int requesttype = C_OUT;
	int request =  0x0c;
	int value = 0x00d0;
	int index = 0x0;
	char buffer[512];
	int bytes = 4;
	int timeout = 5000;
	int ret;
	memset(buffer,0x0,512);
	//20008000
	buffer[0]=0x20;
	buffer[1]=0x00;
	buffer[2]=0x80;
	buffer[3]=0x00;

	ret = usb_control_msg(dev, requesttype, request, value, index, buffer, bytes, timeout);
	if (ret >=0)
		printf("read buffer %s ret: %i\n",buffer,ret);
	else
		printf("error code no %i ret\n",ret);
}

void print_key_status(usb_dev_handle *dev) {
	int requesttype = C_IN;
	int request =  0x0c;
	int value = 0x002d;
	int index = 0x0;
	char buffer[512];
	int bytes = 1;
	int timeout = 5000;
	int ret;

	ret = usb_control_msg(dev, requesttype, request, value, index, buffer, bytes, timeout);
	if (ret >=0)
		printf("read buffer %s ret: %i\n",buffer,ret);
	else
		printf("error code no %i ret\n",ret);

}

int main(void) {
	usb_dev_handle *dev = NULL; /* the device handle */
	uint8_t read_buffer[512];
	int ret =0;

	/*Set up USB */
	usb_init();
	usb_find_busses();
	usb_find_devices();

	if (!(dev = open_dev())) {
	perror("device not found!\n");
		exit(EXIT_FAILURE);
	}

	if (usb_claim_interface(dev, 0) < 0) {
		perror("claiming interface failed\n");
		usb_close(dev);
		exit(EXIT_FAILURE);
	}


	print_COF1(dev);
	print_COF2(dev);
	//print_CIF1(dev);
	print_key_status(dev);

	/* Clean up */
	usb_release_interface(dev, 0);
	usb_close(dev);
	exit(0);
}
