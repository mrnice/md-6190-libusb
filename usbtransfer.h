#ifndef USBTRANSFER_H
#define USBTRANSFER_H

#include <usb.h>

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

/* Primax Electronics, Ltd Medion MD 6190 Scanner USB */
#define MD_6190_VID 0x0461
#define MD_6190_PID 0x037b

/* the device's endpoints */
#define EP_IN 0x81
#define EP_OUT 0x02

usb_dev_handle *open_dev(void);
#endif
